//
//  MyClass2.swift
//  TestFramework
//
//  Created by Pawel Walicki on 23/01/2019.
//  Copyright © 2019 com.ebikemotion. All rights reserved.
//

import Foundation
import Nimble

class MyClass2 {
    
 static func rest(number1: Int, number2: Int) -> Int{
        return number1 - number2
    }
}
