//
//  ViewController.swift
//  CheckFramework
//
//  Created by Pawel Walicki on 23/01/2019.
//  Copyright © 2019 com.ebikemotion. All rights reserved.
//

import UIKit
import TestFramework

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
 
        let myClass = MyClass()
    
        let result = myClass.sum(number1: 5, number2: 7)
    
        print(result)
    }


}

